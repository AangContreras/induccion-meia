# Material del curso de Inducción del Macroentrenamiento en Inteligencia Artificial

 Este repositorio es un clon del repositorio original:
 [Induccion_MeIA](https://github.com/jugernaut/Induccion_MeIA). Contiene
 diversos materiales de consulta y actividades para aprender los fundamentos
 matemáticos y de programación de la inteligencia artificial.

Este repositorio es de ***sólo lectura***.